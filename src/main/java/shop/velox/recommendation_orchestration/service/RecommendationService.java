package shop.velox.recommendation_orchestration.service;

import shop.velox.recommendation.api.dto.RecommendationServiceDto;
import shop.velox.recommendation.api.dto.TrackingDto;

public interface RecommendationService {

  RecommendationServiceDto getRecommendationByCategory(String categoryId);

  void createTrackingEntry(TrackingDto trackingDto);

}
