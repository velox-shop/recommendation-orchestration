package shop.velox.recommendation_orchestration.service;

import org.springframework.http.ResponseEntity;
import shop.velox.recommendation.api.dto.TrackingDto;
import shop.velox.recommendation_orchestration.api.dto.RecommendationDto;


public interface RecommendationOrchestrationService {

  /**
   * Gets recommendation for category
   *
   * @param categoryId of the recommended articles
   * @return the recommendation for desired category
   */
  ResponseEntity<RecommendationDto> getRecommendationByCategory(String categoryId, int length,
      String currencyIsoCode);

  /**
   * Creates tracking entry
   *
   * @param orderId represents the orderId of the articles for which the tracking should be created
   * @return the status code of the tracking creation
   */
  ResponseEntity<Void> createTrackingEntriesForOrder(String orderId);

  /**
   * Creates tracking entry
   *
   * @param trackingDto represents the tracking information of an article
   * @return the status code of the tracking creation
   */
  ResponseEntity<Void> createTrackingEntry(TrackingDto trackingDto);

}
