package shop.velox.recommendation_orchestration.service;

import shop.velox.catalog_orchestration.api.dto.EnrichedProductDto;

public interface CatalogService {

  EnrichedProductDto getEnrichedProduct(String itemId, String currencyIsoCode);
}
