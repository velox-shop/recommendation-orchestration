package shop.velox.recommendation_orchestration.service;

import shop.velox.order.api.dto.OrderDto;

public interface OrderService {

  OrderDto getOrder(String orderId);

}
