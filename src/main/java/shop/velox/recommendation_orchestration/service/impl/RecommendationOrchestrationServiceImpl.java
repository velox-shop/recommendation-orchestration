package shop.velox.recommendation_orchestration.service.impl;

import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.springframework.http.HttpStatus.CREATED;

import java.util.List;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import shop.velox.catalog_orchestration.api.dto.EnrichedProductDto;
import shop.velox.order.api.dto.OrderDto;
import shop.velox.order.api.dto.OrderEntryDto;
import shop.velox.recommendation.api.dto.RecommendationServiceDto;
import shop.velox.recommendation.api.dto.TrackingDto;
import shop.velox.recommendation_orchestration.api.dto.RecommendationDto;
import shop.velox.recommendation_orchestration.service.CatalogService;
import shop.velox.recommendation_orchestration.service.OrderService;
import shop.velox.recommendation_orchestration.service.RecommendationOrchestrationService;
import shop.velox.recommendation_orchestration.service.RecommendationService;
import shop.velox.user.api.dto.UserDto;

@Service
@RequiredArgsConstructor
public class RecommendationOrchestrationServiceImpl implements RecommendationOrchestrationService {

  private final CatalogService catalogService;
  private final OrderService orderService;
  private final RecommendationService recommendationService;

  @Override
  public ResponseEntity<RecommendationDto> getRecommendationByCategory(String categoryId,
      int length, String currencyIsoCode) {

    RecommendationDto recommendationOrchestrationDto = new RecommendationDto();

    RecommendationServiceDto recommendationServiceDto = recommendationService.getRecommendationByCategory(
        categoryId);
    if (recommendationServiceDto != null) {

      recommendationOrchestrationDto.setCategoryId(recommendationServiceDto.getCategoryId());

      // enrich recommended articles
      List<EnrichedProductDto> recommendedDetailedItems = emptyIfNull(
          recommendationServiceDto.getArticleIds())
          .stream()
          .limit(length)
          .map(itemId -> catalogService.getEnrichedProduct(itemId, currencyIsoCode))
          .filter(Objects::nonNull)
          .toList();

      recommendationOrchestrationDto.setArticles(recommendedDetailedItems);
    }

    return ResponseEntity.ok()
        .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS)
        .body(recommendationOrchestrationDto);

  }

  @Override
  public ResponseEntity<Void> createTrackingEntriesForOrder(String orderId) {

    OrderDto orderDto = orderService.getOrder(orderId);

    UserDto userDto = orderDto.getUser();
    String userId = userDto != null ? userDto.getId() : null;
    String cartId = orderDto.getCartId();
    orderDto.getEntries()
        .forEach(orderEntry -> trackOrderEntry(orderId, userId, cartId, orderEntry));

    return new ResponseEntity<>(CREATED);
  }

  private void trackOrderEntry(String orderId, String userId, String cartId,
      OrderEntryDto orderEntry) {
    TrackingDto trackingDto = new TrackingDto();
    //for now, all items belong to the same category, when item category is implemented
    //category should be retrieved from the item
    trackingDto.setCategoryId("123");
    trackingDto.setArticleId(orderEntry.getArticleId());
    trackingDto.setUserId(userId);
    trackingDto.setAction("order");
    trackingDto.setCartId(cartId);
    trackingDto.setOrderId(orderId);
    trackingDto.setQuantity(orderEntry.getQuantity());

    createTrackingEntry(trackingDto);
  }

  @Override
  public ResponseEntity<Void> createTrackingEntry(TrackingDto trackingDto) {
    recommendationService.createTrackingEntry(trackingDto);
    return ResponseEntity.status(CREATED).build();
  }

}
