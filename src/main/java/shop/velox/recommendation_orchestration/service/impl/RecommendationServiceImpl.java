package shop.velox.recommendation_orchestration.service.impl;

import static org.springframework.http.HttpStatus.CREATED;

import java.net.URI;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.recommendation.api.dto.RecommendationServiceDto;
import shop.velox.recommendation.api.dto.TrackingDto;
import shop.velox.recommendation_orchestration.service.RecommendationService;

@Service
@RequiredArgsConstructor
@Slf4j
public class RecommendationServiceImpl implements RecommendationService {

  private final RestTemplate restTemplate;

  @Value("${recommendation.url}")
  private String recommendationUrl;

  @Override
  public RecommendationServiceDto getRecommendationByCategory(String categoryId) {
    if (StringUtils.isBlank(categoryId)) {
      log.warn("getRecommendationByCategory with missing categoryId");
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    URI uri = UriComponentsBuilder.fromUriString(recommendationUrl)
        .pathSegment("recommendations")
        .pathSegment("categories")
        .path(categoryId)
        .build()
        .toUri();
    log.info("getRecommendationByCategory with URI: {}", uri);
    ResponseEntity<RecommendationServiceDto> responseRecommendationDto = restTemplate.getForEntity(
        uri, RecommendationServiceDto.class);
    log.debug("getRecommendation status: {},  body: {}",
        responseRecommendationDto.getStatusCode(),
        responseRecommendationDto.getBody());

    if (!responseRecommendationDto.getStatusCode().is2xxSuccessful()) {
      throw new ResponseStatusException(responseRecommendationDto.getStatusCode());
    }

    return responseRecommendationDto.getBody();
  }

  @Override
  public void createTrackingEntry(TrackingDto trackingDto) {
    if (trackingDto == null) {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY);
    }
    URI uri = UriComponentsBuilder.fromUriString(recommendationUrl)
        .path("/trackings")
        .build()
        .toUri();
    ResponseEntity<TrackingDto> responseTrackingDto = restTemplate.postForEntity(uri,
        trackingDto,
        TrackingDto.class);
    if (!CREATED.equals(responseTrackingDto.getStatusCode())) {
      log.error("created tracking status: {},  body: {}", responseTrackingDto.getStatusCode(),
          responseTrackingDto.getBody());
      throw new ResponseStatusException(responseTrackingDto.getStatusCode());
    }
  }
}
