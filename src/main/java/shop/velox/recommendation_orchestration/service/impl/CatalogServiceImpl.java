package shop.velox.recommendation_orchestration.service.impl;

import java.net.URI;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.catalog_orchestration.api.dto.EnrichedProductDto;
import shop.velox.recommendation_orchestration.service.CatalogService;

@Service
@Slf4j
@RequiredArgsConstructor
public class CatalogServiceImpl implements CatalogService {

  private final RestTemplate restTemplate;

  @Value("${catalogorchestration.url}")
  private String catalogOrchestrationUrl;

  @Override
  public EnrichedProductDto getEnrichedProduct(String itemId, String currencyIsoCode) {
    URI uri = UriComponentsBuilder.fromUriString(catalogOrchestrationUrl)
        .pathSegment("products")
        .path(itemId)
        .queryParamIfPresent("currencyIsoCode", Optional.ofNullable(currencyIsoCode))
        .build()
        .toUri();
    log.info("getEnrichedProduct with URI: {}", uri);
    ResponseEntity<EnrichedProductDto> responseItemDto = restTemplate.getForEntity(uri,
        EnrichedProductDto.class);
    log.debug("detailed itemId: {}, status: {}, body: {}", itemId,
        responseItemDto.getStatusCode(),
        responseItemDto.getBody());

    if (!responseItemDto.getStatusCode().is2xxSuccessful()) {
      log.error("fetching detailed item with id: {}, resulted in error {}", itemId,
          responseItemDto.getStatusCode());
      return null;
    }
    return responseItemDto.getBody();
  }

}
