package shop.velox.recommendation_orchestration.service.impl;

import java.net.URI;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.order.api.dto.OrderDto;
import shop.velox.recommendation_orchestration.service.OrderService;

@Service
@Slf4j
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

  private final RestTemplate restTemplate;

  @Value("${checkoutorchestration.url}")
  private String checkoutOrchestrationUrl;

  @Override
  public OrderDto getOrder(String orderId) {
    if (StringUtils.isBlank(orderId)) {
      log.warn("getOrder with missing orderId");
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    URI uri = UriComponentsBuilder.fromUriString(checkoutOrchestrationUrl)
        .path("/orders/")
        .path(orderId)
        .build()
        .toUri();
    log.info("getOrder with URI: {}", uri);
    ResponseEntity<OrderDto> responseOrderDto = restTemplate.getForEntity(uri, OrderDto.class);
    log.debug("retrieved order: {}, status: {}, body: {}", orderId,
        responseOrderDto.getStatusCode(), responseOrderDto.getBody());

    if (!responseOrderDto.getStatusCode().is2xxSuccessful()) {
      throw new ResponseStatusException(responseOrderDto.getStatusCode(),
          "Error retrieving order with id: " + orderId);
    }

    var orderDto = responseOrderDto.getBody();
    if (orderDto == null) {
      throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
          "Problem retrieving Order with id:" + orderId);
    }

    return orderDto;
  }

}
