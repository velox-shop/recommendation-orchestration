package shop.velox.recommendation_orchestration.api.dto;

import static java.time.LocalDateTime.now;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import lombok.Data;
import shop.velox.catalog_orchestration.api.dto.EnrichedProductDto;

@Data
public class RecommendationDto {

  @Schema(
      description = "Unique identifier of the Recommendation.",
      example = "3b228r9df8-574e-694b-153a-66uu178j80i8"
  )
  @JsonInclude(JsonInclude.Include.ALWAYS)
  private String id;

  @Schema(
      description = "Unique identifier of the Category.",
      example = "001"
  )
  @JsonInclude(JsonInclude.Include.ALWAYS)
  private String categoryId;

  @Schema(
      description = "List of the Recommended Items with details (name, quantity, price, availability, ...)",
      example = "[{id: 2e5c9ba8-956e-476b-816a-49ee128a40c9, name: 'example-article, availability: {...}, ... }, {...}]"
  )
  @JsonInclude(JsonInclude.Include.ALWAYS)
  private List<EnrichedProductDto> articles;

  @Schema(
      description = "Timestamp when the recommendation was generated.",
      example = "10-03-28 04:26:38,647"
  )
  @JsonInclude(JsonInclude.Include.ALWAYS)
  private LocalDateTime recommendationTimestamp = LocalDateTime.now().withNano(0);

  public RecommendationDto() {
    super();
    this.id = UUID.randomUUID().toString();
    this.recommendationTimestamp = now().withNano(0);
  }

}
