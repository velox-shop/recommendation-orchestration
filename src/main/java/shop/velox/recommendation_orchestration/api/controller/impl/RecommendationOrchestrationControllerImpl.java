package shop.velox.recommendation_orchestration.api.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import shop.velox.recommendation.api.dto.TrackingDto;
import shop.velox.recommendation_orchestration.api.controller.RecommendationOrchestrationController;
import shop.velox.recommendation_orchestration.api.dto.RecommendationDto;
import shop.velox.recommendation_orchestration.service.RecommendationOrchestrationService;

@RestController
public class RecommendationOrchestrationControllerImpl implements
    RecommendationOrchestrationController {

  private static final Logger LOG = LoggerFactory.getLogger(
      RecommendationOrchestrationControllerImpl.class);

  private final RecommendationOrchestrationService recommendationOrchestrationService;

  public RecommendationOrchestrationControllerImpl(
      @Autowired RecommendationOrchestrationService recommendationOrchestrationService) {
    this.recommendationOrchestrationService = recommendationOrchestrationService;
  }

  @Override
  public ResponseEntity<RecommendationDto> getRecommendationForCategory(String categoryId,
      int length, String currencyIsoCode) {
    LOG.info("get Recommendation for category: {}", categoryId);
    return recommendationOrchestrationService.getRecommendationByCategory(categoryId, length,
        currencyIsoCode);
  }

  @Override
  public ResponseEntity<Void> createTrackingEntry(TrackingDto trackingDto) {
    LOG.info("creating tracking entry: {}", trackingDto);
    return recommendationOrchestrationService.createTrackingEntry(trackingDto);
  }

  @Override
  public ResponseEntity<Void> createTrackingEntriesForOrder(String orderId) {
    LOG.info("creating tracking entries for order: {}", orderId);
    return recommendationOrchestrationService.createTrackingEntriesForOrder(orderId);
  }
}
