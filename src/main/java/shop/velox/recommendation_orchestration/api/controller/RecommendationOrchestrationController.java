package shop.velox.recommendation_orchestration.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import shop.velox.recommendation.api.dto.TrackingDto;
import shop.velox.recommendation_orchestration.api.dto.RecommendationDto;

@Tag(name = "Recommendation", description = "the Recommendation-Orchestration API")
public interface RecommendationOrchestrationController {

  @Operation(summary = "gets Recommendation for category", description = "Paginated.")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200",
          description = "successful operation",
          content = @Content(schema = @Schema(implementation = RecommendationDto.class)))
  })
  @GetMapping(value = "/recommendations/categories/{categoryId}", produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<RecommendationDto> getRecommendationForCategory(
      @Parameter(description = "Category of the recommended articles.")
      @PathVariable String categoryId,
      @Parameter(description = "Number of recommended articles in the recommendation. Can be empty. Default 3")
      @RequestParam(defaultValue = "3", required = false) int length,
      @Parameter(description = "Currency isoCode of the recommended articles. Can be empty. If empty, returns prices for recommended articles in all available currencies.")
      @RequestParam(required = false) String currencyIsoCode);


  @Operation(summary = "Creates a Tracking Entry", description = "")
  @PostMapping(value = "/trackings", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201",
          description = "Tracking Entry created.",
          content = @Content(schema = @Schema(implementation = TrackingDto.class))),
      @ApiResponse(responseCode = "422",
          description = "TrackingDto is missing.",
          content = @Content(schema = @Schema(implementation = TrackingDto.class)))
  })
  ResponseEntity<Void> createTrackingEntry(
      @Parameter(description = "Tracking Entry used to create tracking.")
      @RequestBody TrackingDto trackingDto);

  @Operation(summary = "Creates a Tracking Entries for articles in the order", description = "")
  @PostMapping(value = "/trackings/orders/{orderId}", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201",
          description = "Tracking Entries created.",
          content = @Content(schema = @Schema(implementation = TrackingDto.class))),
      @ApiResponse(responseCode = "404",
          description = "OrderId is missing/order with specified orderId does not exist",
          content = @Content(schema = @Schema(implementation = TrackingDto.class)))
  })
  ResponseEntity<Void> createTrackingEntriesForOrder(
      @Parameter(description = "OrderId of the ordered articles for which the tracking entries should be created. Cannot be empty.")
      @PathVariable final String orderId);
}
