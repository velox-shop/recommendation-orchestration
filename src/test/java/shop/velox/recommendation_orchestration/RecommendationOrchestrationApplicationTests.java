package shop.velox.recommendation_orchestration;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles({"localauth"})
class RecommendationOrchestrationApplicationTests {

  @Test
  void contextLoads() {
  }

}
