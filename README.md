# Recommendation-Orchestration

This repository presents the orchestration service for the recommendation service.

## Work locally

Build Service

```
./gradlew clean build --refresh-dependencies
```

Execute dependencies with Docker Compose and this service with Gradle:

```
docker-compose --file docker-compose.dependencies.yml up
./gradlew bootRun
```

Execute everything with Docker Compose

```
docker-compose --file docker-compose.dependencies.yml --file docker-compose.this.yml up --build --force-recreate --remove-orphans --renew-anon-volumes
```

Test

```
newman run src/test/*postman_collection.json --environment src/test/veloxRecommendationOrchestration-local.postman_environment.json --reporters cli,html --reporter-html-export newman-results.html
```

## API documentation

Online API Documentation is at:

- HTML: http://localhost:8453/recommendation-orchestration/v1/swagger-ui.html
- JSON: http://localhost:8453/recommendation-orchestration/v1/api-docs

Offline API Documentation is at https://velox-shop.gitlab.io/recommendation-orchestration/

### Recommendation-Orchestration Service images

|Version      | Description                                                                                                                                                                                 |
|-------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 2.0.0       | BREAKING CHANGE: ItemDto in RecommendationDto is replaced with EnrichedProductEntity from new catalog-service:1.0 (recommendation_orchestration-api Jar is not backward compatible)         |
| 1.0.2       | Use recommendation 2.0                                                                                                                                                                      |
| 1.0.1       | Change categoryId in create tracking for Order                                                                                                                                              |
| 1.0.0 (1.0) | Base image                                                                                                                                                                                  |
